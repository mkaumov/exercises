#include <iostream>
#include <vector>
#include <string>

#include "Classes/Fraction.h"
#include "Classes/Vector.h"
#include "Classes/VectorMatrix.h"

#include "Classes/Vehicle/Car.h"
#include "Classes/Vehicle/Bicycle.h"
#include "Classes/Vehicle/Engine.h"
#include "Classes/Vehicle/WaterVehicle.h"
#include "Classes/Vehicle/Wheel.h"

#include "Classes/Patterns/Singleton/GameSettings.h"
#include "Classes/Patterns/Prototype/Shape.h"
#include "Classes/Patterns/Builder/Director.h"
#include "Classes/Patterns/Builder/StarshipBuilder.h"
#include "Classes/Patterns/AbstractFactory/Weapon.h"
#include "Classes/Patterns/AbstractFactory/WeaponFactory.h"
#include "Classes/Patterns/Adapter/FileLoader.h"
#include "Classes/Patterns/Adapter/ThirdPartyFileLoaderAdapter.h"
#include "Classes/Patterns/Bridge/GameObjectRenderer.h"
#include "Classes/Patterns/Bridge/GameObject.h"
#include "Classes/Patterns/Command/MusicPlayer.h"
#include "Classes/Patterns/Command/MusicPlayerInvoker.h"
#include "Classes/Patterns/State/TVState.h"
#include "Classes/Patterns/Composite/Menu.h"
#include "Classes/Patterns/Composite/MenuItem.h"
#include "Classes/Patterns/Facade/LightsaberFacade.h"
#include "Classes/Patterns/Flyweight/ParticleFactory.h"
#include "Classes/Patterns/Iterator/AssetContainer.h"
#include "Classes/Patterns/Iterator/AssetIterator.h"
#include "Classes/Patterns/Observer/DisplayDevice.h"
#include "Classes/Patterns/Observer/WeatherData.h"
#include "Classes/Patterns/Proxy/FileAccessProxy.h"
#include "Classes/Patterns/Visitor/Visitor.h"
#include "Classes/Patterns/Visitor/VisitorObjectStructural.h"

float getHighestPower(const std::vector<Vehicle*>& vehicles) {
  float HighestPower = 0;
  for (const auto vehicle : vehicles) {
    const auto car = dynamic_cast<Car*>(vehicle);
    if (car && car->GetEnginePower() > HighestPower) {
      HighestPower = car->GetEnginePower();
    }
  }

  return HighestPower;
}

void PrintExampleTitle(std::string&& title) {
  std::cout << "--\n";
  std::cout << title << ":\n";
  std::cout << "--\n";
}

void Exercise_1() {
  const Vector v1{12.5, 4.5, 9.8};
  std::cout << "Vector 1: " << v1 << std::endl;
  const Vector v2{7.5, 5.5, 10.2};
  std::cout << "Vector 2: " << v2 << std::endl;

  std::cout << "----------------" << std::endl;

  Vector v3 = v1 + v2;
  Vector v4 = v1 - v2;

  std::cout << "Vector 3 (sum of v1 and v2): " << v3 << std::endl;
  std::cout << "Vector 3 length: " << static_cast<double>(v3) << std::endl;
  std::cout << "Vector 4 (dif of v1 and v2): " << v4 << std::endl;
  std::cout << "Vector 4 length: " << static_cast<double>(v4) << std::endl;

  std::cout << "Vector 3 multiplied by 2: " << (v3 * 2.0) << std::endl;
  std::cout << "Vector 4 multiplied by 5: " << (v4 * 2.0) << std::endl;

  std::cout << "----------------" << std::endl;

  Vector v5;
  std::cin >> v5;
  std::cout << "Vector 5: " << v5 << std::endl;
}

void Exercise_2() {
  const auto m = new VectorMatrix{2, 2};
  m->ShowInfo();

  std::cout << "----------------------------------------" << std::endl;
  const auto c1 = new VectorMatrix{*m};
  c1->ShowInfo();

  std::cout << "----------------------------------------" << std::endl;
  const auto c2 = new VectorMatrix{3, 3};
  c2->ShowInfo();
  *c2 = *m;
  c2->ShowInfo();

  std::cout << "----------------------------------------" << std::endl;
  m->Change(4, 2);
  c2->Change(6, 3);

  m->ShowInfo();
  c1->ShowInfo();
  c2->ShowInfo();

  delete m;
  delete c1;
  delete c2;
}

void Exercise_3() {
  const Car car(new Engine(150), new Wheel(17), new Wheel(17), new Wheel(18), new Wheel(18), 150);
  std::cout << car << '\n';

  const Bicycle bicycle(new Wheel(20), new Wheel(20), 300);
  std::cout << bicycle << "\n---------------------------------------------\n";

  std::vector<Vehicle*> vehicles;
  vehicles.push_back(new Car(new Engine(150), new Wheel(17), new Wheel(17), new Wheel(18), new Wheel(18), 250));
  vehicles.push_back(new Car(new Engine(200), new Wheel(19), new Wheel(19), new Wheel(19), new Wheel(19), 130));
  vehicles.push_back(new WaterVehicle(5000));

  for (const auto vehicle : vehicles) std::cout << *vehicle << '\n';
  std::cout << "The highest power is " << getHighestPower(vehicles) << '\n';
  for (const auto vehicle : vehicles) delete vehicle;
}

void Exercise_4() {
  int tries = 0;

  while (tries < 3) {
    std::cout << "Enter the numerator and the denominator:" << '\n';

    float numerator, denominator;
    std::cin >> numerator;
    std::cin >> denominator;

    try {
      Fraction fraction(numerator, denominator);
      std::cout << "Result of fraction: " << fraction.GetResult();
      return;
    }
    catch (std::exception& exception) {
      std::cerr << "Error: " << exception.what() << " Try again...\n\n";
      tries++;
    }
  }

  std::cerr << "The denominator can be equal zero only in math limits." << '\n';
}

void Exercise_5() {
  GameSettings* settings = GameSettings::GetInstance();

  settings->SetSoundVolume(100);
  settings->SetFullScreen(true);

  std::cout << "---- Singleton example ----\n\n";
  std::cout << "Current game settings:\n";
  std::cout << "Sound volume: " << settings->GetSoundVolume() << "; Fullscreen mode: " << (settings->GetFullScreen()
    ? "enabled"
    : "disabled");

  std::cout << "\n-------------------------------------------------------\n";
  std::cout << "\n---- Prototype example ----\n";
  const auto Circle1 = new Circle{5};
  Circle* Circle2 = Circle1->Clone();
  std::cout << "Circle 1: ";
  Circle1->Draw();
  std::cout << "Circle 2: ";
  Circle2->Draw();
  std::cout << "Circle 1 != Circle 2: " << (&Circle1 != &Circle2 ? "true" : "false");

  std::cout << "\n-------------------------------------------------------\n";
  std::cout << "\n---- Builder example ----\n";
  Director director;

  director.SetBuilder(new MillenniumFalconBuilder);
  const auto MillenniumFalcon = director.BuildMillenniumFalcon();
  std::cout << "Starship \"" << MillenniumFalcon->GetName() << "\"\n";
  std::cout << "Type: " << MillenniumFalcon->GetType() << '\n';
  std::cout << "Faction: " << MillenniumFalcon->GetFaction() << '\n';
  std::cout << "Crew count: " << MillenniumFalcon->GetCrewCount() << '\n';
  std::cout << "Length: " << MillenniumFalcon->GetLength() << '\n';

  director.SetBuilder(new TIEFighterBuilder);
  const auto TIEFighter = director.BuildTIEFighter();
  std::cout << "Starship \"" << TIEFighter->GetName() << "\"\n";
  std::cout << "Type: " << TIEFighter->GetType() << '\n';
  std::cout << "Faction: " << TIEFighter->GetFaction() << '\n';
  std::cout << "Crew count: " << TIEFighter->GetCrewCount() << '\n';
  std::cout << "Length: " << TIEFighter->GetLength() << '\n';

  std::cout << "-------------------------------------------------------\n";
  std::cout << "---- Abstract factory example ----\n";

  std::cout << "Rebel factory\n";
  WeaponFactory* RebelFactory = new RebelWeaponFactory();
  Weapon* RebelBlaster = RebelFactory->CreateBlaster();
  std::cout << "Rebel blaster: ";
  RebelBlaster->Fire();
  std::cout << "Rebel lightsaber: ";
  Weapon* RebelLightsaber = RebelFactory->CreateLightsaber();
  RebelLightsaber->Fire();

  std::cout << "Imperial factory\n";
  WeaponFactory* ImperialFactory = new ImperialWeaponFactory();
  Weapon* ImperialBlaster = ImperialFactory->CreateBlaster();
  std::cout << "Imperial blaster: ";
  ImperialBlaster->Fire();
  std::cout << "Imperial lightsaber: ";
  Weapon* ImperialLightsaber = ImperialFactory->CreateLightsaber();
  if (ImperialLightsaber) {
    ImperialLightsaber->Fire();
  }
  else {
    std::cout << "Imperials don't use lightsaber\n";
  }
}

void Exercise_6() {
#define FACADE_EXAMPLE
#ifdef FACADE_EXAMPLE
  {
    PrintExampleTitle("Facade example");
    const auto lightsaber = std::make_unique<LightsaberFacade>();
    lightsaber->On();
    lightsaber->Off();
  }
#endif

#define PROXY_EXAMPLE
#ifdef PROXY_EXAMPLE
  {
    PrintExampleTitle("Proxy example");
    const auto writer = std::make_unique<FileAccessProxy>(AccessedRole::Writers);
    writer->Write("Lorem ipsum...");
    writer->Read();
    const auto rider = std::make_unique<FileAccessProxy>(AccessedRole::Riders);
    rider->Write("Lorem ipsum...");
    rider->Read();
  }
#endif

#define COMPOSITE_EXAMPLE
#ifdef COMPOSITE_EXAMPLE
  {
    PrintExampleTitle("Composite example");
    const auto submenu1 = std::make_unique<Menu>("Sub menu 1", "First level sub menu element.");
    submenu1->Add({new MenuItem("Sub menu 1 Item 1", "Second level menu item element.", 2500)});

    const auto submenu2 = std::make_unique<Menu>("Sub menu 2", "First level sub menu element.");
    submenu2->Add({new MenuItem("Sub menu 2 Item 1", "Second level menu item element.", 3500)});

    const auto menu = std::make_unique<Menu>("Main menu", "Zero level menu.");
    menu->Add({
      submenu1.get(),
      submenu2.get(),
      new MenuItem("Main menu Item 1", "First level menu item element.", 1500),
    });
    menu->Print();
  }
#endif

#define ADAPTER_EXAMPLE
#ifdef ADAPTER_EXAMPLE
  {
    PrintExampleTitle("Adapter example");
    const auto fileLoader = std::make_unique<FileLoader>();
    fileLoader->LoadFile("path/file.ext");
    fileLoader->GetData();

    const auto fileLoaderAdapter = std::make_unique<ThirdPartyFileLoaderAdapter>();
    fileLoaderAdapter->LoadFile("path/third_party_file.ext");
    fileLoaderAdapter->GetData();
  }
#endif

#define BRIDGE_EXAMPLE
#ifdef BRIDGE_EXAMPLE
  {
    PrintExampleTitle("Bridge example");

    const auto openGLRenderer = std::make_shared<OpenGLRenderer>();
    const auto directXRenderer = std::make_shared<DirectXRenderer>();

    const auto object = std::make_unique<GameObject>(openGLRenderer);
    object->Render();
    object->SetRenderer(directXRenderer);
    object->Render();
  }
#endif

#define FLYWEIGHT_EXAMPLE
#ifdef FLYWEIGHT_EXAMPLE
  {
    PrintExampleTitle("Flyweight example");

    const std::initializer_list<ParticleStruct> ParticleStructs{
      ParticleStruct{100.0, 46.50, 34.30, "t1.texture"},
      ParticleStruct{2.300, 12.12, 54.30, "t2.texture"},
      ParticleStruct{12.30, 123.5, 10.30, "t3.texture"},
      ParticleStruct{32.30, 21.50, 2.300, "t1.texture"},
      ParticleStruct{6.345, 3.500, 43.30, "t1.texture"},
      ParticleStruct{72.90, 54.50, 3.300, "t1.texture"},
      ParticleStruct{74.89, 78.50, 54.30, "t2.texture"},
      ParticleStruct{12.54, 9.500, 6.400, "t2.texture"},
      ParticleStruct{5.340, 0.510, 110.3, "t1.texture"},
      ParticleStruct{97.73, 108.2, 98.12, "t3.texture"},
      ParticleStruct{56.67, 43.23, 67.45, "t2.texture"},
      ParticleStruct{132.4, 25.45, 87.60, "t1.texture"},
      ParticleStruct{45.30, 37.87, 78.67, "t3.texture"}
    };
    const auto Particles = ParticleFactory::CreateParticles(ParticleStructs);

    std::cout << "Particle count: " << ParticleStructs.size() << '\n';
    std::cout << "Particle texture count: " << ParticleFactory::GetTextureMapSize() << '\n';

    for (const auto Particle : Particles) {
      Particle->Draw();
    }
  }
#endif
}

void Exercise_7() {
#define COMMAND_EXAMPLE
#ifdef COMMAND_EXAMPLE
  {
    PrintExampleTitle("Command example");

    MusicPlayer player;
    MusicPlayerInvoker playerInvoker;

    playerInvoker.SetCommand(new PlayCommand(&player));
    playerInvoker.ExecuteCommand();
    playerInvoker.SetCommand(new PauseCommand(&player));
    playerInvoker.ExecuteCommand();
    playerInvoker.SetCommand(new PlayCommand(&player));
    playerInvoker.ExecuteCommand();
    playerInvoker.SetCommand(new StopCommand(&player));
    playerInvoker.ExecuteCommand();
  }
#endif

#define STATE_EXAMPLE
#ifdef STATE_EXAMPLE
  {
    PrintExampleTitle("State example");
    const TVContext Context;
    Context.PressButton();
    Context.PressButton();
    Context.PressButton();
  }
#endif

#define VISITOR_EXAMPLE
#ifdef VISITOR_EXAMPLE
  {
    PrintExampleTitle("Visitor example");

    VisitorObjectStructural ObjectStructure;
    ObjectStructure.Attach(new VisitorRectangle(40, 100));
    ObjectStructure.Attach(new VisitorCircle(16));

    AreaVisitor AreaVisitor;
    ObjectStructure.Accept(AreaVisitor);
    std::cout << "Total area: " << AreaVisitor.GetArea() << std::endl;

    PerimeterVisitor PerimeterVisitor;
    ObjectStructure.Accept(PerimeterVisitor);
    std::cout << "Total perimeter: " << PerimeterVisitor.GetPerimeter() << std::endl;
  }
#endif

#define OBSERVER_EXAMPLE
#ifdef OBSERVER_EXAMPLE
  {
    PrintExampleTitle("Observer example");
    WeatherData weatherData;
    weatherData.AddObserver(new ConsoleDisplay);
    weatherData.AddObserver(new GUIDisplay);
    weatherData.SetMeasurements(25.0, 1013.25);
  }
#endif

#define ITERATOR_EXAMPLE
#ifdef ITERATOR_EXAMPLE
  {
    PrintExampleTitle("Iterator example");

    AssetContainer Container;
    Container.Add(new Asset{"Lightsaber"});
    Container.Add(new Asset{"Blaster"});

    AssetIterator It = Container.Begin();
    while (It.Next()) {
      std::cout << "Asset: " << It.Current()->GetName() << std::endl;
    }
  }
#endif
}

int main() {
  // srand(time(nullptr));

  // Exercise_1();
  // Exercise_2();
  // Exercise_3();
  // Exercise_4();
  // Exercise_5();
  // Exercise_6();
  Exercise_7();

  // system("pause");

  return 0;
}
