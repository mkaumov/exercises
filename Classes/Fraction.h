﻿#pragma once
#include <stdexcept>

class Fraction
{
public:
  Fraction(float numerator, float denominator) {
    if (denominator == 0.0f) {
      throw std::runtime_error("denominator must be not equal to 0.");
    }
    
    this->numerator = numerator;
    this->denominator = denominator;
  }

  float GetResult() {
    return numerator / denominator;
  }
private:
  float numerator;
  float denominator;
};
