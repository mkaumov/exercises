﻿#include "VectorMatrix.h"
#include "Vector.h"
#include <iostream>

VectorMatrix::VectorMatrix(int p_rows, int p_columns) {
  Init(p_rows, p_columns);
  Fill();
}

VectorMatrix::~VectorMatrix() {
  Clear();
}

VectorMatrix::VectorMatrix(const VectorMatrix& other) {
  Clear();
  Init(*other.rows, *other.columns);
  Fill(other.data);
}

VectorMatrix& VectorMatrix::operator =(const VectorMatrix& other) {
  Clear();
  Init(*other.rows, *other.columns);
  Fill(other.data);
  return *this;
}

void VectorMatrix::ShowInfo() const {
  std::cout << "rows: " << *rows << "; columns: " << *columns << std::endl;  
  std::cout << "data: [" << std::endl;

  for (int i = 0; i < *rows; i++) {
    std::cout << "  ";
    
    for (int j = 0; j < *columns; j++) {
      std::cout << *data[i][j] << " ";
    }

    std::cout << std::endl;
  }

  std::cout << "]" << std::endl;
}

void VectorMatrix::Change(int p_rows, int p_columns) {
  Clear();
  Init(p_rows, p_columns);
  Fill();
}

void VectorMatrix::Init(int p_rows, int p_columns) {
  rows = new int{std::max(p_rows, 1)};
  columns = new int{std::max(p_columns, 1)};  
  data = new Vector**[*rows];
}

void VectorMatrix::Fill(Vector*** otherData) {
  for (int i = 0; i < *rows; i++) {
    data[i] = new Vector*[*columns];

    for (int j = 0; j < *columns; j++) {
      if (otherData) {
        const Vector* v = otherData[i][j];
        data[i][j] = new Vector{*v};
      } else {
        data[i][j] = new Vector{double(rand() % 10), double(rand() % 10), double(rand() % 10)};
      }
    }
  }
}

void VectorMatrix::Clear() {
  if (data) {
    for (int i = 0; i < *rows; i++) {
      for (int j = 0; j < *columns; j++) {
        delete data[i][j];
      }
      delete[] data[i];
    }
    delete[] data;
  }

  delete rows;
  delete columns;
}
