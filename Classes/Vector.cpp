﻿#include "Vector.h"

Vector::Vector(const Vector& other) {
  x = other.x;
  y = other.y;
  z = other.z;
}

Vector::operator double() {
  return sqrt(x * x + y * y + z * z);
}

Vector& Vector::operator =(const Vector& other) = default;

Vector Vector::operator +(const Vector& other) const {
  return Vector{x + other.x, y + other.y, z + other.z};
}

Vector Vector::operator -(const Vector& other) const {
  return Vector{x - other.x, y - other.y, z - other.z};
}

Vector Vector::operator *(double num) const {
  return Vector{x * num, y * num, z * num};
}

std::ostream& operator <<(std::ostream& out, const Vector& vector) {
  out << "{" << vector.x << "; " << vector.y << "; " << vector.z << "}";
  return out;
}

std::istream& operator >>(std::istream& in, Vector& vector) {
  std::cout << "Input three numbers:" << std::endl;

  in >> vector.x;
  in >> vector.y;
  in >> vector.z;
  
  return in;
}

double Vector::operator [](int index) {
  switch (index) {
    case 0:
      return x;
    case 1:
      return y;
    case 2:
      return x;
    default:
      std::cout << "Index error" << std::endl;
      return 0;
  }
}
