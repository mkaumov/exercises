﻿#include "RoadVehicle.h"

#include <iostream>

RoadVehicle::RoadVehicle(float RideHeight) {
  this->RideHeight = RideHeight;
}

std::ostream& RoadVehicle::print(std::ostream& out) const {
  out << "Ride height: " << RideHeight;
  return out;
}
