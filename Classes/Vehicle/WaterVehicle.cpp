﻿#include "WaterVehicle.h"

WaterVehicle::WaterVehicle(float Draft) {
  this->Draft = Draft;
}

std::ostream& WaterVehicle::print(std::ostream& out) const {
  out << "WaterVehicle Draft: " << Draft;
  return out;
}
