﻿#pragma once

class Point
{
public:
  float x = 0;
  float y = 0;
  float z = 0;
  
  Point(float x, float y, float z) : x{x}, y{y}, z{z} {}
};
