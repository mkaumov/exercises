﻿#pragma once
#include "Engine.h"
#include "RoadVehicle.h"

class Wheel;
class Engine;

class Car : public RoadVehicle
{
public:
  Car(Engine* engine, Wheel* wheel1, Wheel* wheel2, Wheel* wheel3, Wheel* wheel4, float RideHeight);
  ~Car() override;
  
  std::ostream& print(std::ostream& out) const override;

  float GetEnginePower() const { return engine->Power; }

private:
  Engine* engine = nullptr;
  Wheel** wheels = nullptr;
};
