﻿#pragma once
#include "Vehicle.h"

class RoadVehicle : public Vehicle
{
public:
  RoadVehicle(float RideHeight);
  
  std::ostream& print(std::ostream& out) const override;

private:
  float RideHeight;  
};
