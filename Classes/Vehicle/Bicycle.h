﻿#pragma once
#include "RoadVehicle.h"

class Wheel;

class Bicycle : public RoadVehicle
{
public:
  Bicycle(Wheel* wheel1, Wheel* wheel2, float RideHeight);
  ~Bicycle() override;

  std::ostream& print(std::ostream& out) const override;

private:
  Wheel** wheels = nullptr;
};
