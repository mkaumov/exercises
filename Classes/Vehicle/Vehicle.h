﻿#pragma once

#include <ostream>

class Vehicle
{
public:
  virtual ~Vehicle() = default;

  friend std::ostream& operator <<(std::ostream& out, const Vehicle& vehicle) {
    return vehicle.print(out);
  }

  virtual std::ostream& print(std::ostream& out) const = 0;
};
