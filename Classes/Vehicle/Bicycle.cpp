﻿#include "Bicycle.h"
#include "Wheel.h"

Bicycle::Bicycle(Wheel* wheel1, Wheel* wheel2, float RideHeight) : RoadVehicle(RideHeight) {
  this->wheels = new Wheel*[]{wheel1, wheel2};
}

Bicycle::~Bicycle() {
  for (int i = 0; i < 2; i++) {
    delete this->wheels[i];
  }

  delete[] this->wheels;
}

std::ostream& Bicycle::print(std::ostream& out) const {
  out << "Bicycle Wheels: ";

  for (int i = 0; i < 2; i++) {
    out << wheels[i]->Diameter << " ";
  }
  
  return RoadVehicle::print(out);
}
