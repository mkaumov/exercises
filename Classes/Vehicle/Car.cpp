﻿#include "Car.h"
#include "Engine.h"
#include "Wheel.h"

Car::Car(Engine* engine, Wheel* wheel1, Wheel* wheel2, Wheel* wheel3, Wheel* wheel4, float RideHeight) : RoadVehicle(RideHeight) {
  this->engine = engine;
  this->wheels = new Wheel*[]{wheel1, wheel2, wheel3, wheel4};
}

Car::~Car() {
  delete this->engine;

  for (int i = 0; i < 4; i++) {
    delete this->wheels[i];
  }

  delete[] this->wheels;
}

std::ostream& Car::print(std::ostream& out) const {
  out << "Car Engine: " << engine->Power << " Wheels: ";

  for (int i = 0; i < 4; i++) {
    out << wheels[i]->Diameter << " ";
  }
  
  return RoadVehicle::print(out);
}
