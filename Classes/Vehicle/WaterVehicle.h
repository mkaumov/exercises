﻿#pragma once
#include "Vehicle.h"

class WaterVehicle : public Vehicle
{
public:
  WaterVehicle(float Draft);
  
  std::ostream& print(std::ostream& out) const override;

private:
  float Draft;
};
