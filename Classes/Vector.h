﻿#pragma once
#include <iostream>

class Vector
{
public:
  Vector() { }

  Vector(const double x, const double y, const double z) :
    x{x}, y{y}, z{z} { }

  Vector(const Vector& other);

  operator double();

  Vector& operator =(const Vector& other);

  Vector operator +(const Vector& other) const;

  Vector operator -(const Vector& other) const;

  Vector operator *(double num) const;

  friend std::ostream& operator <<(std::ostream& out, const Vector& vector);

  friend std::istream& operator >>(std::istream& in, Vector& vector);

  double operator [](int index);

private:
  double x = 0;
  double y = 0;
  double z = 0;
};
