﻿#pragma once

class Vector;

class VectorMatrix
{
public:
  VectorMatrix(int p_rows, int p_columns);
  ~VectorMatrix();

  VectorMatrix(const VectorMatrix& other);

  VectorMatrix& operator =(const VectorMatrix& other);

  void ShowInfo() const;
  void Change(int p_rows, int p_columns);
  
private:
  int* rows{};
  int* columns{};
  Vector*** data{};

  void Init(int p_rows, int p_columns);
  void Fill(Vector*** otherData = nullptr);
  void Clear();
};
