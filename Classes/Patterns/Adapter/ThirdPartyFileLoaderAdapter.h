﻿#pragma once
#include "IFileLoader.h"
#include "ThirdPartyFileLoader.h"

class ThirdPartyFileLoaderAdapter : public IFileLoader, ThirdPartyFileLoader
{
public:
  ThirdPartyFileLoaderAdapter() {
    thirdPartyFileLoader = new ThirdPartyFileLoader();
  }

  ~ThirdPartyFileLoaderAdapter() override {
    delete thirdPartyFileLoader;
  }

  void LoadFile(std::string Filename) override {
    thirdPartyFileLoader->ThirdPartyLoadFile(Filename);
  }

  std::string GetData() override {
    return thirdPartyFileLoader->ThirdPartyGetData();
  }

private:
  ThirdPartyFileLoader* thirdPartyFileLoader;
};
