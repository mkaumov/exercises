﻿#pragma once
#include <string>

class IFileLoader
{
public:
  virtual ~IFileLoader() = default;
  virtual void LoadFile(std::string Filename) = 0;
  virtual std::string GetData() = 0;
};
