﻿#pragma once
#include <iostream>

class ThirdPartyFileLoader
{
public:
  void ThirdPartyLoadFile(const std::string& Filename) {
    std::cout << "Third party loading file: " << Filename << std::endl;
    ThirdPartyData = "Third party file data...";
  }

  std::string ThirdPartyGetData() {
    std::cout << "Third party getting file data: \"" << ThirdPartyData << "\"\n";
    return ThirdPartyData;
  }

private:
  std::string ThirdPartyData;
};
