﻿#pragma once
#include <iostream>

#include "IFileLoader.h"

class FileLoader : public IFileLoader
{
public:
  void LoadFile(const std::string Filename) override {
    std::cout << "Loading file: " << Filename << std::endl;
    Data = "File data...";
  }

  std::string GetData() override {
    std::cout << "Getting file data: \"" << Data << "\"\n";
    return Data;
  }

private:
  std::string Data;
};
