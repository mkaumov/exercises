﻿#pragma once

#include <string>

#include "BladeComponent.h"
#include "HandleComponent.h"

class LightsaberFacade
{
public:
  LightsaberFacade() {
    Blade = new BladeComponent;
    Handle = new HandleComponent;
  }

  virtual ~LightsaberFacade() {
    delete Blade;
    delete Handle;
  }

  LightsaberFacade(const LightsaberFacade& other) {
    Handle = new HandleComponent;
    Blade = new BladeComponent;
  }

  LightsaberFacade& operator=(const LightsaberFacade& other) {
    if (this == &other) return *this;

    Handle = new HandleComponent;
    Blade = new BladeComponent;

    return *this;
  }

  LightsaberFacade(LightsaberFacade&& other) noexcept :
    Handle{other.Handle},
    Blade{other.Blade} {

    other.Handle = nullptr;
    other.Blade = nullptr;
  }

  LightsaberFacade& operator=(LightsaberFacade&& other) noexcept {
    if (this == &other) return *this;

    Handle = other.Handle;
    Blade = other.Blade;

    other.Handle = nullptr;
    other.Blade = nullptr;

    return *this;
  }

  virtual void On() {
    std::cout << "[LightsaberFacade]: Enabling" << '\n';

    Handle->ProcessState(State::On);
    Blade->Show();
  }

  virtual void Off() {
    std::cout << "[LightsaberFacade]: Disabling" << '\n';

    Handle->ProcessState(State::Off);
    Blade->Hide();
  }

private:
  HandleComponent* Handle = nullptr;
  BladeComponent* Blade = nullptr;
};
