﻿#pragma once

#include <iostream>

class BladeComponent
{
public:
  virtual void Show() {
    std::cout << "[LightsaberFacade]: BladeComponent is visible." << '\n';
  }

  virtual void Hide() {
    std::cout << "[LightsaberFacade]: BladeComponent is hidden." << '\n';
  }

  virtual ~BladeComponent() = default;
};
