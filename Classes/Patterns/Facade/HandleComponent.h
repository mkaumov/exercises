﻿#pragma once

#include <iostream>

enum class State { On, Off };

class HandleComponent
{
public:
  virtual void ProcessState(const State state) {
    std::cout << "[LightsaberFacade]: HandleComponent changes self state by " << (state == State::On ? "\"On\"" : "\"Off\"") << " state." <<
      '\n';
  }

  virtual void GetMesh() {
    std::cout << "[LightsaberFacade]: HandleComponent return mesh." << '\n';
  }

  virtual ~HandleComponent() = default;
};
