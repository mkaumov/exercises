﻿#pragma once
#include <iostream>

class GameObject
{
public:
  
  GameObject(std::shared_ptr<GameObjectRenderer> Renderer):
    Renderer(std::move(Renderer)) {}

  virtual ~GameObject() {
    Renderer.reset();
  }

  void SetRenderer(std::shared_ptr<GameObjectRenderer> renderer) {
    std::swap(renderer, Renderer);
  }
  
  virtual void Update() {
    std::cout << "[GameObject]: updating..." << std::endl;
  }

  virtual void Render() {
    std::cout << "[GameObject]: rendering..." << std::endl;
    Renderer->Render(*this);
  }

private:
  std::shared_ptr<GameObjectRenderer> Renderer;
};
