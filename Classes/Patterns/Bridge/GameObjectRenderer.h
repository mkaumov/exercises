﻿#pragma once
#include <iostream>

class GameObject;

class GameObjectRenderer
{
public:
  virtual ~GameObjectRenderer() = default;
  virtual void Render(const GameObject& Object) = 0;
};

class GameObjectRendererBridge : public GameObjectRenderer
{
public:
  void Render(const GameObject& Object) override {
    RenderGameObject(Object);
  }

protected:
  virtual void RenderGameObject(const GameObject& Object) = 0;
};

class OpenGLRenderer : public GameObjectRendererBridge {
protected:
  void RenderGameObject(const GameObject& Object) override {
    std::cout << "[OpenGLRenderer]: rendering..." << std::endl;
  }
};

class DirectXRenderer : public GameObjectRendererBridge {
protected:
  void RenderGameObject(const GameObject& Object) override {
    std::cout << "[DirectXRenderer]: rendering..." << std::endl;
  }
};
