﻿#pragma once

#include "Weapon.h"

class WeaponFactory
{
public:
  virtual Weapon* CreateBlaster() = 0;
  virtual Weapon* CreateLightsaber() = 0;
};

class RebelWeaponFactory : public WeaponFactory
{
public:
  Weapon* CreateBlaster() {
    return new Blaster(10, 100);
  }
  
  Weapon* CreateLightsaber() {
    return new Lightsaber(15, 50, "Green");
  }
};

class ImperialWeaponFactory : public WeaponFactory
{
public:
  Weapon* CreateBlaster() {
    return new Blaster(20, 150);
  }
  
  Weapon* CreateLightsaber() {
    return nullptr;
  }
};
