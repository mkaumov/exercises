﻿#pragma once

#include <iostream>
#include <string>

class Weapon
{
public:
  virtual void Fire() = 0;
};

class Blaster : public Weapon
{
public:
  Blaster(int Damage, int Range) :
    Damage(Damage), Range(Range) {}

  void Fire() override {
    std::cout << "Pew Pew! Deals " << Damage << " damage and can reach " << Range << " meters" << std::endl;
  }

private:
  int Damage = 0;
  int Range = 0;
};

class Lightsaber : public Weapon
{
public:
  Lightsaber(int Damage, int Speed, std::string Color) :
    Damage(Damage), Speed(Speed), Color(std::move(Color)) {}

  void Fire() override {
    std::cout << "Vrrrrrrrrm! Deals " << Damage << " damage with a swing speed of " << Speed << " and its color is " << Color << std::endl;
  }

private:
  int Damage = 0;
  int Speed = 0;
  std::string Color = "blue";
};
