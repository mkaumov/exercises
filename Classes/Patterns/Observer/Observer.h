﻿#pragma once

class Observer {
public:
  virtual ~Observer() = default;
  virtual void Update(double temperature, double pressure) = 0;
};
