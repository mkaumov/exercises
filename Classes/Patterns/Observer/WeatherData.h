﻿#pragma once
#include <vector>
#include "Observer.h"

class WeatherData {
public:
  void AddObserver(Observer* Observer) {
    Observers.push_back(Observer);
  }

  void RemoveObserver(const Observer* Observer) {
    const auto Position = std::find(Observers.begin(), Observers.end(), Observer);
    if (Position != Observers.end()) {
      Observers.erase(Position);
    }
  }

  void NotifyObservers() const {
    for (const auto Observer : Observers) {
      Observer->Update(Temperature, Pressure);
    }
  }

  void SetMeasurements(double temperature, double pressure) {
    this->Temperature = temperature;
    this->Pressure = pressure;
    NotifyObservers();
  }

private:
  std::vector<Observer*> Observers;
  double Temperature = 0.0;
  double Pressure = 0.0;
};
