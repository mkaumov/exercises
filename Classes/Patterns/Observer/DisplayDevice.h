﻿#pragma once
#include <iostream>
#include "Observer.h"

class DisplayDevice : public Observer {
public:
  void Update(double temperature, double pressure) final {
    Display(temperature, pressure);
  }
  
  virtual void Display(double temperature, double pressure) = 0;
};

class ConsoleDisplay : public DisplayDevice {
public:
  void Display(double temperature, double pressure) override {
    std::cout << "[ConsoleDisplay] Temperature: " << temperature << "C" << std::endl;
    std::cout << "[ConsoleDisplay] Pressure: " << pressure << " hPa" << std::endl;
  }
};

class GUIDisplay : public DisplayDevice {
public:
  void Display(double temperature, double pressure) override {
    std::cout << "[GUIDisplay] Temperature: " << temperature << "C" << std::endl;
    std::cout << "[GUIDisplay] Pressure: " << pressure << " hPa" << std::endl;
  }
};
