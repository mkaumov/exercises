﻿#pragma once
#include <iostream>

#include "ParticleStruct.h"
#include "ParticleTexture.h"

class ParticleTexture;

class Particle
{
public:
  Particle(const ParticleStruct& Struct, ParticleTexture* Texture) : Texture(Texture) {
    Coords[0] = Struct.X;
    Coords[1] = Struct.Y;
    Coords[2] = Struct.Z;
  }

  void Draw() const {
    std::cout << "[Particle]: ";
    std::cout << "draw at { " << Coords[0] << ";" << Coords[1] << ";" << Coords[2] << " }";
    std::cout << " with " << Texture->GetTexture() << " texture." << std::endl;
  }

private:
  double Coords[3]{0.0, 0.0, 0.0};
  ParticleTexture* Texture;
};
