﻿#pragma once
#include <unordered_map>

#include "Particle.h"
#include "ParticleTexture.h"

class ParticleFactory
{
  typedef std::unordered_map<std::string, ParticleTexture*> ParticleTextureMap;
  
public:
  static std::vector<Particle*> CreateParticles(std::initializer_list<ParticleStruct> Structs) {
    std::vector<Particle*> Particles{};
    
    for (const ParticleStruct& Struct : Structs) {
      ParticleTexture* Texture;
      
      auto ExistTexture = TextureMap.find<ParticleTexture*>(Struct.TextureName);
      if (ExistTexture != TextureMap.end()) {
        Texture = ExistTexture->second;
      }
      else {
        Texture = new ParticleTexture;
        Texture->SetTexture(Struct.TextureName);
        TextureMap.insert({Struct.TextureName, Texture});
      }
      
      Particles.push_back(new Particle{Struct, Texture}); 
    }

    return Particles;
  }

  static int GetTextureMapSize() {
    return TextureMap.size();
  }
  
private:
  static std::unordered_map<std::string, ParticleTexture*> TextureMap;
};

std::unordered_map<std::string, ParticleTexture*> ParticleFactory::TextureMap{};
