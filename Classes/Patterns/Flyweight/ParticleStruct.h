﻿#pragma once
#include <string>

struct ParticleStruct
{
  double X;
  double Y;
  double Z;
  std::string TextureName;
};
