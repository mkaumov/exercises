﻿#pragma once
#include <string>

class ParticleTexture
{
public:
  std::string GetTexture() {
    return Texture;
  }

  void SetTexture(std::string texture) {
    this->Texture = std::move(texture);
  }

private:
  std::string Texture;
};
