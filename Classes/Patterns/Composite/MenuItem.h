﻿#pragma once
#include <iostream>

#include "MenuComponent.h"

class MenuItem : public MenuComponent
{
public:
  MenuItem(std::string Name, std::string Description, double Value) :
    Name(std::move(Name)), Description(std::move(Description)), Value(Value) {}

  std::string GetName() const override {
    return Name;
  }
  
  std::string GetDescription() const override {
    return Description;
  }
  
  double GetValue() const override {
    return Value;
  }

  void Print() const override {
    // std::cout << "MenuItem Element" << std::endl;
    // std::cout << '{' << std::endl;
    // std::cout << "  Name: " << GetName() << std::endl;
    // std::cout << "  Description: " << GetDescription() << std::endl;
    // std::cout << "  Value: " << GetValue() << std::endl;
    // std::cout << '}' << std::endl;

    std::cout << "MenuItem Element" << std::endl;
    std::cout << "{ ";
    std::cout << "Name: " << GetName();
    std::cout << "; ";
    std::cout << "Description: " << GetDescription();
    std::cout << "; ";
    std::cout << "Value: " << GetValue();
    std::cout << " }" << std::endl;
  }

private:
  std::string Name;
  std::string Description;
  double Value;
};
