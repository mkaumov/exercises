﻿#pragma once
#include <iostream>
#include <vector>

#include "MenuComponent.h"

class Menu : public MenuComponent
{
public:
  Menu(std::string Name, std::string Description) :
    Name(std::move(Name)), Description(std::move(Description)) {}

  void Add(MenuComponent* Component) override {
    MenuComponents.push_back(Component);
  }

  void Add(std::initializer_list<MenuComponent*> Components) override {
    for (auto Component : Components) {
      MenuComponents.push_back(Component);
    }
  }

  void Remove(MenuComponent* Component) override {
    const auto ExistComponent = std::find(MenuComponents.begin(), MenuComponents.end(), Component);

    if (ExistComponent != MenuComponents.end()) {
      MenuComponents.erase(ExistComponent);
    }
  }

  MenuComponent* GetChild(const int i) const override {
    return MenuComponents[i];
  }

  std::string GetName() const override {
    return Name;
  }

  std::string GetDescription() const override {
    return Description;
  }

  void Print() const override {
    std::cout << "Menu Element" << std::endl;
    std::cout << "{ ";
    std::cout << "Name: " << GetName() << "; ";
    std::cout << "Description: " << GetDescription();
    std::cout << " }" << std::endl;

    for (const auto Component : MenuComponents) {
      Component->Print();
    }
  }

private:
  std::string Name;
  std::string Description;
  std::vector<MenuComponent*> MenuComponents;
};
