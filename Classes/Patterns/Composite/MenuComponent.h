﻿#pragma once
#include <string>

class MenuComponent
{
public:
  virtual ~MenuComponent() = default;
  
  virtual void Add(MenuComponent* Component) {}
  virtual void Add(std::initializer_list<MenuComponent*> Components) {}
  virtual void Remove(MenuComponent* Component) {}

  virtual double GetValue() const {
    return 0.0;
  }
  virtual std::string GetName() const {
    return "";
  }
  virtual std::string GetDescription() const {
    return "";
  }
  virtual MenuComponent* GetChild(int i) const {
    return nullptr;
  }

  virtual void Print() const {}
};
