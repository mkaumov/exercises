﻿#pragma once

#include "StarshipBuilder.h"
#include "Starship.h"

class Director
{
public:
  void SetBuilder(StarshipBuilder* Builder) {
    this->Builder = Builder;
  }

  Starship* BuildTIEFighter() {
    Builder = new TIEFighterBuilder();
    Builder->BuildName();
    Builder->BuildFaction();
    Builder->BuildType();
    Builder->BuildCrew();
    Builder->BuildLength();
    return Builder->GetStarship();
  }

  Starship* BuildMillenniumFalcon() {
    Builder = new MillenniumFalconBuilder();
    Builder->BuildName();
    Builder->BuildFaction();
    Builder->BuildType();
    Builder->BuildCrew();
    Builder->BuildLength();
    return Builder->GetStarship();
  }
  
private:
  StarshipBuilder* Builder = nullptr;
};
