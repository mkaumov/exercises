﻿#pragma once

#include <string>

class Starship
{
public:
  Starship() = default;

  void SetName(const std::string Name) {
    this->Name = Name;
  }

  void SetFaction(const std::string Faction) {
    this->Faction = Faction;
  }

  void SetType(const std::string Type) {
    this->Type = Type;
  }

  void SetCrewCount(const int CrewCount) {
    this->CrewCount = CrewCount;
  }

  void SetLength(const int Length) {
    this->Length = Length;
  }

  std::string GetName() const {
    return Name;
  }

  std::string GetFaction() const {
    return Faction;
  }

  std::string GetType() const {
    return Type;
  }

  int GetCrewCount() const {
    return CrewCount;
  }

  int GetLength() const {
    return Length;
  }

private:
  std::string Name = "unknown";
  std::string Faction = "unknown";
  std::string Type = "unknown";
  int CrewCount = 0;
  int Length = 0;
};
