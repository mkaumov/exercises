﻿#pragma once

#include "Starship.h"

class StarshipBuilder
{
protected:
  Starship* starship;

public:
  StarshipBuilder() {
    starship = new Starship();
  }

  virtual ~StarshipBuilder() {
    delete starship;
  }

  virtual void BuildName() = 0;

  virtual void BuildFaction() = 0;

  virtual void BuildType() = 0;

  virtual void BuildCrew() = 0;

  virtual void BuildLength() = 0;

  Starship* GetStarship() const {
    return starship;
  }
};

class TIEFighterBuilder : public StarshipBuilder
{
public:
  void BuildName() override {
    starship->SetName("TIE Fighter");
  }

  void BuildFaction() override {
    starship->SetFaction("Galactic Empire");
  }

  void BuildType() override {
    starship->SetType("Starfighter");
  }

  void BuildCrew() override {
    starship->SetCrewCount(1);
  }

  void BuildLength() override {
    starship->SetLength(9);
  }
};

class MillenniumFalconBuilder : public StarshipBuilder
{
public:
  void BuildName() override {
    starship->SetName("Millennium Falcon");
  }

  void BuildFaction() override {
    starship->SetFaction("Rebel Alliance");
  }

  void BuildType() override {
    starship->SetType("Freighter");
  }

  void BuildCrew() override {
    starship->SetCrewCount(6);
  }

  void BuildLength() override {
    starship->SetLength(34);
  }
};
