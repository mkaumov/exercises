﻿#pragma once
#include <iostream>
#include "IProxiedFile.h"

class ProxiedFile : public IProxiedFile
{
public:
  std::string Read() override {
    std::cout << "[ProxiedFile]: read file: \"" << FileText << '\"' << std::endl;
    return FileText;
  }

  void Write(const std::string text) override {
    std::cout << "[ProxiedFile]: write file: \"" << text << '\"' << std::endl;
    FileText = text;
  }

private:
  std::string FileText = "Starting text.";
};
