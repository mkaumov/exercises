﻿#pragma once
#include <string>

class IProxiedFile
{
public:
  virtual ~IProxiedFile() = default;
  virtual std::string Read() = 0;
  virtual void Write(std::string text) = 0;
};
