﻿#pragma once
#include <iostream>
#include <set>

#include "IProxiedFile.h"
#include "ProxiedFile.h"

enum class AccessedRole
{
  Riders = 1,
  Writers = 2,
  Admin = 3,
};

std::ostream& operator<<(std::ostream& out, AccessedRole role) {
  out << static_cast<std::underlying_type_t<AccessedRole>>(role);
  return out;
}

class FileAccessProxy : public IProxiedFile
{
public:
  FileAccessProxy(AccessedRole Role) {
    File = new ProxiedFile{};
    CurrentRole = Role;
  }

  std::string Read() override {
    std::cerr << "[FileAccessProxy]: attempting to read this file with role: " << CurrentRole << std::endl;
    
    if (CurrentRole < AccessedRole::Riders) {
      std::cerr << "[FileAccessProxy]: you have not permissions to read this file." << std::endl;
      return "";
    }

    return File->Read();
  }

  void Write(const std::string text) override {
    std::cerr << "[FileAccessProxy]: attempting to write this file with role: " << CurrentRole << std::endl;
    
    if (CurrentRole < AccessedRole::Writers) {
      std::cerr << "[FileAccessProxy]: you have not permissions to write this file." << std::endl;
      return;
    }

    File->Write(text);
  }

protected:
  AccessedRole CurrentRole;
  ProxiedFile* File;
};
