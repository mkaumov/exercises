﻿#pragma once

class GameSettings {
public:
  static GameSettings* GetInstance() {
    if (!Instance) {
      Instance = new GameSettings();
    }
    
    return Instance;
  }
  
  int GetSoundVolume() const {
    return SoundVolume;
  }
  void SetSoundVolume(int Value) {
    SoundVolume = Value;
  }

  bool GetFullScreen() const {
    return FullScreen;
  }
  void SetFullScreen(bool value) {
    FullScreen = value;
  }
  
  GameSettings(const GameSettings&) = delete;
  GameSettings& operator =(const GameSettings&) = delete;
  
private:
  static GameSettings* Instance;
  
  int SoundVolume;
  bool FullScreen;
  
  GameSettings() {
    SoundVolume = 50;
    FullScreen = false;
  }
};

GameSettings* GameSettings::Instance = nullptr;
