﻿#pragma once
#include <vector>
#include "Asset.h"

class AssetIterator;

class AssetContainer {
public:
  void Add(Asset* Asset) {
    Assets.push_back(Asset);
  }

  void Remove(const Asset* Asset) {
    const auto Position = std::find(Assets.begin(), Assets.end(), Asset);
    if (Position != Assets.end()) {
      Assets.erase(Position);
    }
  }

  AssetIterator Begin();
  
  std::vector<Asset*> GetAssets() {
    return Assets;
  }

  Asset* GetAsset(size_t Index) const {
    if (Index < GetSize()) {
      return Assets[Index];
    }

    return nullptr;
  }

  size_t GetSize() const {
    return Assets.size();
  }
private:
  std::vector<Asset*> Assets;
};
