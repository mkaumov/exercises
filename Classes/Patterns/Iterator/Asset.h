﻿#pragma once
#include <string>

class Asset {
public:
  Asset(std::string Name) :
    Name(std::move(Name)) {}

  std::string GetName() const {
    return Name;
  }

private:
  std::string Name;
};
