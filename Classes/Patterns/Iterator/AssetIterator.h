﻿#pragma once
#include "Asset.h"
#include "AssetContainer.h"

class AssetIterator {
public:
  AssetIterator(AssetContainer* Container) :
    Container(Container), Index(0) {}

  Asset* Current() const {
    return Container->GetAsset(Index - 1);
  }

  bool Next() {
    if (Index >= Container->GetSize()) {
      return false;
    }

    Index++;
    return true;
  }

private:
  AssetContainer* Container{};
  size_t Index;
};
