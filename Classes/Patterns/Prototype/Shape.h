﻿#pragma once

#include <iostream>

class Shape {
public:
  virtual Shape* Clone() = 0;
  virtual void Draw() = 0;
};

class Circle : public Shape {
public:
  Circle(int R) : Radius(R) {}
  Circle(const Circle& Other) : Radius(Other.Radius) {}

  Circle* Clone() override {
    return new Circle(*this);
  }
  
  void Draw() override {
    std::cout << "Drawing circle with radius: " << Radius << std::endl;
  }

private:
  int Radius;
};

class Square : public Shape {
public:
  Square(int s) : Side(s) {}
  Square(const Square& Other) : Side(Other.Side) {}

  Square* Clone() override {
    return new Square(*this);
  }
  
  void Draw() override {
    std::cout << "Drawing square with side: " << Side << std::endl;
  }

private:
  int Side;
};
