﻿#pragma once

#include "VisitorShape.h"

class Visitor {
public:
  virtual ~Visitor() = default;
  virtual void VisitRectangle(VisitorRectangle& rectangle) = 0;
  virtual void VisitCircle(VisitorCircle& circle) = 0;
};

class AreaVisitor : public Visitor {
public:
  void VisitRectangle(VisitorRectangle& rectangle) override;
  void VisitCircle(VisitorCircle& circle) override;

  double GetArea() const {
    return Area;
  }

private:
  double Area = 0.0;
};

inline void AreaVisitor::VisitRectangle(VisitorRectangle& rectangle) {
  Area += rectangle.GetWidth() * rectangle.GetHeight();
}

inline void AreaVisitor::VisitCircle(VisitorCircle& circle) {
  Area += 3.14 * circle.GetRadius() * circle.GetRadius();
}

class PerimeterVisitor : public Visitor {
public:
  void VisitRectangle(VisitorRectangle& rectangle) override;
  void VisitCircle(VisitorCircle& circle) override;

  double GetPerimeter() const {
    return Perimeter;
  }

private:
  double Perimeter = 0.0;
};

inline void PerimeterVisitor::VisitRectangle(VisitorRectangle& rectangle) {
  Perimeter += 2 * (rectangle.GetWidth() + rectangle.GetHeight());
}

inline void PerimeterVisitor::VisitCircle(VisitorCircle& circle) {
  Perimeter += 2 * 3.14 * circle.GetRadius();
}
