﻿#include "Visitor.h"
#include "VisitorShape.h"

void VisitorRectangle::Accept(Visitor& visitor) {
  visitor.VisitRectangle(*this);
}

void VisitorCircle::Accept(Visitor& visitor) {
  visitor.VisitCircle(*this);
}
