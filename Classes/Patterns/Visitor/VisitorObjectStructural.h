﻿#pragma once
#include <vector>

#include "VisitorShape.h"

class VisitorObjectStructural {
public:
  void Attach(VisitorShape* Shape) {
    Shapes.push_back(Shape);
  }

  void Detach(const VisitorShape* Shape) {
    const auto Position = std::find(Shapes.begin(), Shapes.end(), Shape);
    if (Position != Shapes.end())
      Shapes.erase(Position);
  }
  
  void Accept(Visitor& visitor) const {
    for (VisitorShape* Shape : Shapes) {
      Shape->Accept(visitor);
    }
  }

private:
  std::vector<VisitorShape*> Shapes;
};
