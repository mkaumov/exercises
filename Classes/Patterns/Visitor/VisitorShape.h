﻿#pragma once

class Visitor;

class VisitorShape {
public:
  virtual void Accept(Visitor& visitor) = 0;
};

class VisitorRectangle : public VisitorShape {
public:
  VisitorRectangle(double Width, double Height) :
    Width(Width), Height(Height) {}

  double GetWidth() const {
    return Width;
  }

  double GetHeight() const {
    return Height;
  }

  void Accept(Visitor& visitor) override;

private:
  double Width;
  double Height;
};

class VisitorCircle : public VisitorShape {
public:
  VisitorCircle(double Radius) :
    Radius(Radius) {}

  double GetRadius() const {
    return Radius;
  }

  void Accept(Visitor& visitor) override;

private:
  double Radius;
};
