﻿#pragma once
#include "Command.h"

class MusicPlayerInvoker {
public:
  void SetCommand(Command* command) {
    Command = command;
  }

  void ExecuteCommand() {
    if (Command) {
      Command->Execute();
    }
  }

private:
  Command* Command = nullptr;
};
