﻿#pragma once
#include "MusicPlayer.h"

class Command {
public:
  virtual ~Command() = default;
  virtual void Execute() = 0;
};

class PlayCommand : public Command {
public:
  explicit PlayCommand(MusicPlayer* player) :
    Player(player) {}

  ~PlayCommand() override {
    delete Player;
  }

  void Execute() override {
    Player->Play();
  }

private:
  MusicPlayer* Player;
};

class PauseCommand : public Command {
public:
  explicit PauseCommand(MusicPlayer* Player) :
    Player(Player) {}

  ~PauseCommand() override {
    delete Player;
  }

  void Execute() override {
    Player->Pause();
  }

private:
  MusicPlayer* Player;
};

class StopCommand : public Command {
public:
  explicit StopCommand(MusicPlayer* Player) :
    Player(Player) {}

  ~StopCommand() override {
    delete Player;
  }

  void Execute() override {
    Player->Stop();
  }

private:
  MusicPlayer* Player;
};
