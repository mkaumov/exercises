﻿#pragma once
#include <iostream>

class MusicPlayer {
public:
  void Play() {
    std::cout << "[MusicPlayer] Playing..." << std::endl;
  }

  void Pause() {
    std::cout << "[MusicPlayer] Paused." << std::endl;
  }

  void Stop() {
    std::cout << "[MusicPlayer] Stopped." << std::endl;
  }
};
