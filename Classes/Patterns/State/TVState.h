﻿#pragma once
#include <iostream>

class TVContext;
class TVStateOn;
class TVStateOff;

class TVState {
public:
  virtual ~TVState() = default;
  virtual void PressButton() = 0;
  virtual void SetContext(TVContext* context) { Context = context; }

protected:
  TVContext* Context = nullptr;
};

class TVContext {
public:
  TVContext();

  void SetState(TVState* state) {
    State = state;
    State->SetContext(this);
  }

  void PressButton() const {
    State->PressButton();
  }

private:
  TVState* State;
};

class TVStateOff : public TVState {
public:
  void PressButton() override;
};

class TVStateOn : public TVState {
public:
  void PressButton() override;
};


// чтобы можно было использовать forward declaration  

inline TVContext::TVContext() {
  SetState(new TVStateOff);
}

inline void TVStateOff::PressButton() {
  std::cout << "Turning TV on." << std::endl;
  Context->SetState(new TVStateOn);
  delete this;
}

inline void TVStateOn::PressButton() {
  std::cout << "Turning TV off." << std::endl;
  Context->SetState(new TVStateOff);
  delete this;
}
